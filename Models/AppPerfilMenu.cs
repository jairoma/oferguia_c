﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class AppPerfilMenu
    {
        public int Id { get; set; }
    
        public int AppPerfil { get; set; }
        public int AppMenu { get; set; }

    }
}
