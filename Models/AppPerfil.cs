﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class AppPerfil
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool FlagEliminado { get; set; }
        [StringLength(150)]
        public string UltimoUsuarioModificacion { get; set; }
        public DateTime UltimaFechaModificacion { get; set; }
       
        // relacion con tabla app_perfil_orden
        public int AppPerfilOrdenId { get; set; }

        public AppPerfilOrden AppPerfilOrden { get; set; }

        // app_perfil_menu
        public int AppPerfilMenuId { get; set; }

        public AppPerfilMenu AppPerfilMenu { get; set; }

    }
}
