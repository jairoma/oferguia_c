﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class AppPerfilOrden
    {

        public int Id { get; set; }
  
        public int OrdenPerfil { get; set; }
        public int AppPerfil { get; set; }
        public int AppMenuModulo { get; set; }
        public int AppMenuSubModulo { get; set; }
        public int AppMenuSubModulo2 { get; set; }

        
    }
}
