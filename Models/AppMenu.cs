﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class AppMenu
    {

        public int Id { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }
        [StringLength(150)]
        public string NombreLargo { get; set; }
        [StringLength(100)]
        public string Codigo { get; set; }
        [StringLength(50)]
        public string Ruta { get; set; }
        [StringLength(100)]
        public string Icono { get; set; }
        public bool FlagVisible { get; set; }
        public bool FlagEliminado { get; set; }
        [StringLength(150)]
        public string UltimoUsuarioModificacion { get; set; }
        public DateTime UltimaFechaModificacion { get; set; }
        public int AppMenuPadre { get; set; }
        public int AppEstadoId { get; set; }

        //relacion tabla AppMenu con app_perfil menu
        public int AppPerfilMenuId { get; set; }
        public AppPerfilMenu AppPerfilMenu { get; set; }

        //relacion tabla AppMenu con app_perfil_orden
        public int AppPerfilOrdenId { get; set; }

        public AppPerfilOrden AppPerfilOrden { get; set; }








    }
}
