﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class AppPersona
    {
        public int Id { get; set; }
        
        [StringLength(255)]
        public string Foto { get; set; }
        public DateTime FechaNacimiento { get; set; }
        [StringLength(50)]
        public string NroDocumento { get; set; }
        [StringLength(50)]
        public string Nombre { get; set; }
        [StringLength(150)]
        public string Apellido { get; set; }
        [StringLength(150)]
        public bool Sexo { get; set; }
        public string Direccion { get; set; }
        public bool FlagEliminado { get; set; }
        public bool FlagAcceso { get; set; }
        [StringLength(100)]
        public string CodigoBarra { get; set; }
        [StringLength(150)]
        public string Correo { get; set; }
        [StringLength(255)]
        public string ClavePrimaria { get; set; }
        [StringLength(155)]
        public string ClaveSecundaria { get; set; }
        public DateTime FechaCreacion { get; set; }
        [StringLength(17)]
        public string Ip { get; set; }
        public DateTime UltimaFechaConexion { get; set; }
        [StringLength(150)]
        public string UltimoUsuarioModificacion { get; set; }
        public DateTime UltimaFechaModificacion { get; set; }
        public int AppMunicipioId { get; set; }
        public int AppEstadoId { get; set; }
        public int AppLicenciaId { get; set; }
     


    }
}
