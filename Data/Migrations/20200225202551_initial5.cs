﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Data.Migrations
{
    public partial class initial5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "app_menu");

            migrationBuilder.DropColumn(
                name: "app_estado_id",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "app_licencia_id",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "app_municipio_id",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "clave_primaria",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "clave_secundaria",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "codigo_barra",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "fecha_creacion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "fecha_nacimiento",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "flag_acceso",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "flag_eliminado",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "ultima_fecha_conexion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "ultima_fecha_modificacion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "ultimo_usuario_modificacion",
                table: "app_persona");

            migrationBuilder.RenameColumn(
                name: "sexo",
                table: "app_persona",
                newName: "Sexo");

            migrationBuilder.RenameColumn(
                name: "nroDocumento",
                table: "app_persona",
                newName: "NroDocumento");

            migrationBuilder.RenameColumn(
                name: "nombre",
                table: "app_persona",
                newName: "Nombre");

            migrationBuilder.RenameColumn(
                name: "ip",
                table: "app_persona",
                newName: "Ip");

            migrationBuilder.RenameColumn(
                name: "foto",
                table: "app_persona",
                newName: "Foto");

            migrationBuilder.RenameColumn(
                name: "direccion",
                table: "app_persona",
                newName: "Direccion");

            migrationBuilder.RenameColumn(
                name: "correo",
                table: "app_persona",
                newName: "Correo");

            migrationBuilder.RenameColumn(
                name: "apellido",
                table: "app_persona",
                newName: "Apellido");

            migrationBuilder.AddColumn<int>(
                name: "AppEstadoId",
                table: "app_persona",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AppLicenciaId",
                table: "app_persona",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AppMunicipioId",
                table: "app_persona",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ClavePrimaria",
                table: "app_persona",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClaveSecundaria",
                table: "app_persona",
                maxLength: 155,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CodigoBarra",
                table: "app_persona",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaCreacion",
                table: "app_persona",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaNacimiento",
                table: "app_persona",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "FlagAcceso",
                table: "app_persona",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FlagEliminado",
                table: "app_persona",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UltimaFechaConexion",
                table: "app_persona",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UltimaFechaModificacion",
                table: "app_persona",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UltimoUsuarioModificacion",
                table: "app_persona",
                maxLength: 150,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppPerfilMenu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppPerfil = table.Column<int>(nullable: false),
                    AppMenu = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppPerfilMenu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppPerfilOrden",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrdenPerfil = table.Column<int>(nullable: false),
                    AppPerfil = table.Column<int>(nullable: false),
                    AppMenuModulo = table.Column<int>(nullable: false),
                    AppMenuSubModulo = table.Column<int>(nullable: false),
                    AppMenuSubModulo2 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppPerfilOrden", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppMenu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 100, nullable: true),
                    NombreLargo = table.Column<string>(maxLength: 150, nullable: true),
                    Codigo = table.Column<string>(maxLength: 100, nullable: true),
                    Ruta = table.Column<string>(maxLength: 50, nullable: true),
                    Icono = table.Column<string>(maxLength: 100, nullable: true),
                    FlagVisible = table.Column<bool>(nullable: false),
                    FlagEliminado = table.Column<bool>(nullable: false),
                    UltimoUsuarioModificacion = table.Column<string>(maxLength: 150, nullable: true),
                    UltimaFechaModificacion = table.Column<DateTime>(nullable: false),
                    AppMenuPadre = table.Column<int>(nullable: false),
                    AppEstadoId = table.Column<int>(nullable: false),
                    AppPerfilMenuId = table.Column<int>(nullable: false),
                    AppPerfilOrdenId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppMenu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppMenu_AppPerfilMenu_AppPerfilMenuId",
                        column: x => x.AppPerfilMenuId,
                        principalTable: "AppPerfilMenu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AppMenu_AppPerfilOrden_AppPerfilOrdenId",
                        column: x => x.AppPerfilOrdenId,
                        principalTable: "AppPerfilOrden",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppMenu_AppPerfilMenuId",
                table: "AppMenu",
                column: "AppPerfilMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_AppMenu_AppPerfilOrdenId",
                table: "AppMenu",
                column: "AppPerfilOrdenId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppMenu");

            migrationBuilder.DropTable(
                name: "AppPerfilMenu");

            migrationBuilder.DropTable(
                name: "AppPerfilOrden");

            migrationBuilder.DropColumn(
                name: "AppEstadoId",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "AppLicenciaId",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "AppMunicipioId",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "ClavePrimaria",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "ClaveSecundaria",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "CodigoBarra",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "FechaCreacion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "FechaNacimiento",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "FlagAcceso",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "FlagEliminado",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "UltimaFechaConexion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "UltimaFechaModificacion",
                table: "app_persona");

            migrationBuilder.DropColumn(
                name: "UltimoUsuarioModificacion",
                table: "app_persona");

            migrationBuilder.RenameColumn(
                name: "Sexo",
                table: "app_persona",
                newName: "sexo");

            migrationBuilder.RenameColumn(
                name: "NroDocumento",
                table: "app_persona",
                newName: "nroDocumento");

            migrationBuilder.RenameColumn(
                name: "Nombre",
                table: "app_persona",
                newName: "nombre");

            migrationBuilder.RenameColumn(
                name: "Ip",
                table: "app_persona",
                newName: "ip");

            migrationBuilder.RenameColumn(
                name: "Foto",
                table: "app_persona",
                newName: "foto");

            migrationBuilder.RenameColumn(
                name: "Direccion",
                table: "app_persona",
                newName: "direccion");

            migrationBuilder.RenameColumn(
                name: "Correo",
                table: "app_persona",
                newName: "correo");

            migrationBuilder.RenameColumn(
                name: "Apellido",
                table: "app_persona",
                newName: "apellido");

            migrationBuilder.AddColumn<int>(
                name: "app_estado_id",
                table: "app_persona",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "app_licencia_id",
                table: "app_persona",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "app_municipio_id",
                table: "app_persona",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "clave_primaria",
                table: "app_persona",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "clave_secundaria",
                table: "app_persona",
                type: "nvarchar(155)",
                maxLength: 155,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "codigo_barra",
                table: "app_persona",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "fecha_creacion",
                table: "app_persona",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "fecha_nacimiento",
                table: "app_persona",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "flag_acceso",
                table: "app_persona",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "flag_eliminado",
                table: "app_persona",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ultima_fecha_conexion",
                table: "app_persona",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ultima_fecha_modificacion",
                table: "app_persona",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ultimo_usuario_modificacion",
                table: "app_persona",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "app_menu",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    appMenuPadre = table.Column<int>(type: "int", nullable: false),
                    app_estado_id = table.Column<int>(type: "int", nullable: false),
                    codigo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    flag_eliminado = table.Column<bool>(type: "bit", nullable: false),
                    flag_visible = table.Column<bool>(type: "bit", nullable: false),
                    icono = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    nombre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    nombre_largo = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    ruta = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ultima_fecha_modificacion = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ultimo_usuario_modificacion = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_menu", x => x.Id);
                });
        }
    }
}
