﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Data.Migrations
{
    public partial class MigracionInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "app_menu",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombre = table.Column<string>(maxLength: 100, nullable: true),
                    nombre_largo = table.Column<string>(maxLength: 150, nullable: true),
                    codigo = table.Column<string>(maxLength: 100, nullable: true),
                    ruta = table.Column<string>(maxLength: 50, nullable: true),
                    icono = table.Column<string>(maxLength: 100, nullable: true),
                    flag_visible = table.Column<bool>(nullable: false),
                    flag_eliminado = table.Column<bool>(nullable: false),
                    ultimo_usuario_modificacion = table.Column<string>(maxLength: 150, nullable: true),
                    ultima_fecha_modificacion = table.Column<DateTime>(nullable: false),
                    appMenuPadre = table.Column<int>(nullable: false),
                    app_estado_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "app_persona",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    foto = table.Column<string>(maxLength: 255, nullable: true),
                    fecha_nacimiento = table.Column<DateTime>(nullable: false),
                    nroDocumento = table.Column<string>(maxLength: 50, nullable: true),
                    nombre = table.Column<string>(maxLength: 50, nullable: true),
                    apellido = table.Column<string>(maxLength: 150, nullable: true),
                    sexo = table.Column<bool>(maxLength: 150, nullable: false),
                    direccion = table.Column<string>(nullable: true),
                    flag_eliminado = table.Column<bool>(nullable: false),
                    flag_acceso = table.Column<bool>(nullable: false),
                    codigo_barra = table.Column<string>(maxLength: 100, nullable: true),
                    correo = table.Column<string>(maxLength: 150, nullable: true),
                    clave_primaria = table.Column<string>(maxLength: 255, nullable: true),
                    clave_secundaria = table.Column<string>(maxLength: 155, nullable: true),
                    fecha_creacion = table.Column<DateTime>(nullable: false),
                    ip = table.Column<string>(maxLength: 17, nullable: true),
                    ultima_fecha_conexion = table.Column<DateTime>(nullable: false),
                    ultimo_usuario_modificacion = table.Column<string>(maxLength: 150, nullable: true),
                    ultima_fecha_modificacion = table.Column<DateTime>(nullable: false),
                    app_municipio_id = table.Column<int>(nullable: false),
                    app_estado_id = table.Column<int>(nullable: false),
                    app_licencia_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_persona", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "app_menu");

            migrationBuilder.DropTable(
                name: "app_persona");
        }
    }
}
